#!/bin/bash

device='192.168.59.101:5555'

opt=$1
shift 1
case $opt in
    install|i) ./gradlew installDebug && adb -s $device shell start am -n com.zapata.quantas/.ACTIVITY;;
    log|l) pidcat;; #https://github.com/JakeWharton/pidcat
    test|t) ./gradlew testDebugUnitTest;;
    *) echo "[$opt] not recognised. Options available: (i)nstall, (l)og, (t)est";;
esac
