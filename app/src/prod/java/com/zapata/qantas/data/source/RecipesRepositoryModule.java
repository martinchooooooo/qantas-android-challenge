package com.zapata.qantas.data.source;

import android.app.Application;

import java.util.concurrent.Executors;

import javax.inject.Singleton;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;

/**
 * This is used by Dagger to inject the required arguments into the {@link RecipesRepository}.
 */
@Module
public class RecipesRepositoryModule {

    @Singleton
    @Provides
    RecipesDataSource provideRecipesRemoteDataSource() {
        return new RecipesRemoteDataSource();
    }

}