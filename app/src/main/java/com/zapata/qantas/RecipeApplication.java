package com.zapata.qantas;

import android.support.annotation.VisibleForTesting;

import com.zapata.qantas.data.source.RecipesRepository;
import com.zapata.qantas.di.DaggerAppComponent;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.support.DaggerApplication;

public class RecipeApplication extends DaggerApplication {

    @Inject
    RecipesRepository recipesRepository;

    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        return DaggerAppComponent.builder().application(this).build();
    }

    /**
     /**
     * Our Espresso tests need to be able to get an instance of the {@link RecipesRepository}
     * so that we can delete all tasks before running each test
     */
    @VisibleForTesting
    public RecipesRepository getRecipesRepository() {
        return recipesRepository;
    }
}
