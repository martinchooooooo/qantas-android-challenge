package com.zapata.qantas.recipes;

public interface RecipeClickListener {

    void onItemClick(int itemPosition);
}
