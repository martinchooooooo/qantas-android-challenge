package com.zapata.qantas.recipes;



import android.os.Handler;
import android.os.Looper;

import com.zapata.qantas.data.Recipe;
import com.zapata.qantas.data.source.RecipesDataSource;
import com.zapata.qantas.data.source.RecipesRepository;
import com.zapata.qantas.di.ActivityScoped;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nullable;
import javax.inject.Inject;

@ActivityScoped
public class RecipesPresenter implements RecipesContract.Presenter {

    private final RecipesRepository mRecipesRepository;
    @Nullable
    private RecipesContract.View mRecipeView;

    private List<Recipe> popularRecipes, otherRecipes;

    @Inject
    RecipesPresenter(RecipesRepository recipesRepository) {
        this.mRecipesRepository = recipesRepository;
    }


    @Override
    public void takeView(RecipesContract.View view) {
        this.mRecipeView = view;
    }

    @Override
    public void dropView() { this.mRecipeView = null; }

    private Handler mHandler = new Handler(Looper.getMainLooper());
    @Override
    public void loadRecipes() {
        mRecipesRepository.getRecipes(new RecipesDataSource.LoadRecipesCallback() {
            @Override
            public void onRecipesLoaded(List<Recipe> recipes) {
                popularRecipes = new ArrayList<>();
                otherRecipes = new ArrayList<>();
                // Filter our recipes into popular and others
                for ( Recipe r : recipes ) {
                    if ( r.isPopular() ) {
                        popularRecipes.add(r);
                    } else {
                        otherRecipes.add(r);
                    }
                }

                // The view may not be able to handle UI updates
                if ( mRecipeView == null ) { return; }

                // Show the data on screen
                mRecipeView.showPopularRecipes(popularRecipes);
                mRecipeView.showOtherRecipes(otherRecipes);
            }

            @Override
            public void onFailure() {
                mRecipeView.showLoadFailure();
            }
        });
    }

    @Override
    public void showPopularRecipeDetails(int itemPosition) {
        mRecipeView.openRecipeDetails(popularRecipes.get(itemPosition));
    }

    @Override
    public void showOtherRecipeDetails(int itemPosition) {
        mRecipeView.openRecipeDetails(otherRecipes.get(itemPosition));
    }
}
