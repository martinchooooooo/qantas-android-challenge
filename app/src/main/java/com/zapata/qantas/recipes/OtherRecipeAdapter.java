package com.zapata.qantas.recipes;

import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.zapata.qantas.R;
import com.zapata.qantas.data.Recipe;

import java.lang.ref.WeakReference;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OtherRecipeAdapter extends RecyclerView.Adapter<OtherRecipeAdapter.RowHolder> {

    private List<Recipe> mData;
    private final RecipeClickListener clickListener;

    public OtherRecipeAdapter(List<Recipe> data, RecipeClickListener clickListener) {
        this.mData = data;
        this.clickListener = clickListener;
    }

    @Override
    public RowHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflator = LayoutInflater.from(parent.getContext());
        View view = inflator.inflate(R.layout.row_other_recipe, parent, false);
        return new RowHolder(view, clickListener);
    }

    @Override
    public void onBindViewHolder(RowHolder holder, int position) {
        Recipe r = mData.get(position);
        holder.title.setText(r.getTitle());
        holder.ingredients.setText(TextUtils.join(", ", r.getIngredients()));
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public void setOtherRecipesList(List<Recipe> otherRecipes) {
        this.mData.clear();
        this.mData.addAll(otherRecipes);
        notifyDataSetChanged();
    }

    static class RowHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        @BindView(R.id.txt_title) TextView title;
        @BindView(R.id.txt_ingredients) TextView ingredients;

        private WeakReference<RecipeClickListener> clickListener;

        public RowHolder(View view, RecipeClickListener clickListener) {
            super(view);
            ButterKnife.bind(this, view);

            // Set up all views to listen for onClick
            this.clickListener = new WeakReference<>(clickListener);
            view.setOnClickListener(this);
            title.setOnClickListener(this);
            ingredients.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            this.clickListener.get().onItemClick(getAdapterPosition());
        }
    }
}
