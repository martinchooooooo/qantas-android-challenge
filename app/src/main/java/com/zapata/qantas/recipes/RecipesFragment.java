package com.zapata.qantas.recipes;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.zapata.qantas.R;
import com.zapata.qantas.data.Recipe;
import com.zapata.qantas.di.ActivityScoped;
import com.zapata.qantas.recipedetail.RecipeDetailActivity;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import dagger.android.support.DaggerFragment;

@ActivityScoped
public class RecipesFragment extends DaggerFragment implements RecipesContract.View {

    public static final String RECIPE_ITEM = "RECIPE_ITEM";
    @Inject
    RecipesContract.Presenter mPresenter;

    private PopularRecipeAdapter mpopularAdapter;
    private OtherRecipeAdapter motherAdapter;
    private Unbinder unbinder;

    @BindView(R.id.textView2) TextView mtxtView;
    @BindView(R.id.recV_popularRecipes) RecyclerView mview_popularRecipes;
    @BindView(R.id.recV_otherRecipes) RecyclerView mview_otherRecipes;

    @Inject
    public RecipesFragment() {}

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mpopularAdapter = new PopularRecipeAdapter(new ArrayList<Recipe>(0),
                new RecipeClickListener() {
                    @Override
                    public void onItemClick(int itemPosition) {
                        mPresenter.showPopularRecipeDetails(itemPosition);
                    }
                });
        motherAdapter = new OtherRecipeAdapter(new ArrayList<Recipe>(0),
                new RecipeClickListener() {
                    @Override
                    public void onItemClick(int itemPosition) {
                        mPresenter.showOtherRecipeDetails(itemPosition);
                    }
                });
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.recipes_frag, container, false);

        ButterKnife.bind(this, root);
        //mtxtView.setText("Recipe List");
        mview_popularRecipes.setAdapter(mpopularAdapter);
        mview_otherRecipes.setAdapter(motherAdapter);

        return root;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // Number of columns to display, 2 for portrait, 3 for landscape
        int cols = getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT ? 2 : 3;
        GridLayoutManager popularLayoutManager = new GridLayoutManager(getActivity(), cols);
        mview_popularRecipes.setLayoutManager(popularLayoutManager);

        // Since we are using a LinearLayout, manually add each view
        LinearLayoutManager otherLayMgr = new LinearLayoutManager(getActivity());
        mview_otherRecipes.setLayoutManager(otherLayMgr);

        Fresco.initialize(getActivity());

        mPresenter.takeView(this);
        mPresenter.loadRecipes();
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.takeView(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mPresenter.dropView();
        unbinder.unbind();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        unbinder = ButterKnife.bind(this, view);
    }

    @Override
    public void showPopularRecipes(List<Recipe> popularRecipes) {
        mpopularAdapter.setPopularRecipesList(popularRecipes);
    }

    @Override
    public void showOtherRecipes(List<Recipe> otherRecipes) {
        motherAdapter.setOtherRecipesList(otherRecipes);
    }

    @Override
    public void showLoadFailure() {
        Toast.makeText(this.getContext(), R.string.err_recipesLoad, Toast.LENGTH_LONG).show();
    }

    @Override
    public void openRecipeDetails(Recipe recipe) {
        Intent intent = new Intent(getContext(), RecipeDetailActivity.class);
        intent.putExtra(RECIPE_ITEM, recipe);
        startActivity(intent);
        getActivity().overridePendingTransition(R.anim.slide_left_enter, R.anim.stay);
    }
}
