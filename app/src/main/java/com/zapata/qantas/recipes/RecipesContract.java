/* Taken from  https://github.com/googlesamples/android-architecture/tree/todo-mvp-dagger/ */

package com.zapata.qantas.recipes;

import com.zapata.qantas.BasePresenter;
import com.zapata.qantas.BaseView;
import com.zapata.qantas.data.Recipe;

import java.util.List;

public class RecipesContract {

    interface View extends BaseView<Presenter> {
        void showPopularRecipes(List<Recipe> popularRecipes);
        void showOtherRecipes(List<Recipe> otherRecipes);
        void openRecipeDetails(Recipe recipe);
        void showLoadFailure();
    }

    interface Presenter extends BasePresenter<View> {
        void loadRecipes();
        void showPopularRecipeDetails(int itemPosition);
        void showOtherRecipeDetails(int itemPosition);
    }
}
