package com.zapata.qantas.recipes;

import com.zapata.qantas.di.ActivityScoped;
import com.zapata.qantas.di.FragmentScoped;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class RecipesModule {

    @FragmentScoped
    @ContributesAndroidInjector
    abstract RecipesFragment recipesFragment();

    @ActivityScoped
    @Binds
    abstract RecipesContract.Presenter recipesPresenter(RecipesPresenter presenter);

}
