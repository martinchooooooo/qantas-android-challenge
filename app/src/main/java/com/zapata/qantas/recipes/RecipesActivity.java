package com.zapata.qantas.recipes;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.zapata.qantas.R;
import com.zapata.qantas.util.ActivityUtils;

import javax.inject.Inject;

import dagger.Lazy;
import dagger.android.support.DaggerAppCompatActivity;

public class RecipesActivity extends DaggerAppCompatActivity {

    @Inject
    RecipesPresenter mRecipesPresenter;
    @Inject
    Lazy<RecipesFragment> recipesFragmentProvider;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.recipes_act);

        RecipesFragment recipesFragment = (RecipesFragment) getSupportFragmentManager().findFragmentById(R.id.contentFrame);
        if ( recipesFragment == null ) {
            recipesFragment = recipesFragmentProvider.get();
            ActivityUtils.addFragmentToActivity(
                    getSupportFragmentManager(), recipesFragment, R.id.contentFrame);
        }

    }
}
