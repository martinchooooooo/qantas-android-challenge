package com.zapata.qantas.recipes;

import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.zapata.qantas.R;
import com.zapata.qantas.data.Recipe;

import java.lang.ref.WeakReference;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PopularRecipeAdapter extends RecyclerView.Adapter<PopularRecipeAdapter.GridCellHolder> {

    private final List<Recipe> mData;
    private final RecipeClickListener clickListener;

    public PopularRecipeAdapter(List<Recipe> data, RecipeClickListener clickListener) {
       this.mData = data;
       this.clickListener = clickListener;
    }

    @Override
    public GridCellHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.tile_popular_recipe, parent, false);

        return new GridCellHolder(view, clickListener);
    }

    @Override
    public void onBindViewHolder(GridCellHolder holder, int position) {
        Recipe recipe = mData.get(position);

        holder.title.setText(recipe.getTitle());
        Uri uri = Uri.parse(recipe.getThumbnail());
        holder.thumb.setImageURI(uri);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public void setPopularRecipesList(List<Recipe> popularRecipes) {
        this.mData.clear();
        this.mData.addAll(popularRecipes);
        notifyDataSetChanged();
    }

    static class GridCellHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.txt_title) TextView title;
        @BindView(R.id.img_thumb) SimpleDraweeView thumb;
        private WeakReference<RecipeClickListener> clickListener;

        public GridCellHolder(View view, RecipeClickListener clickListener) {
            super(view);
            ButterKnife.bind(this, view);

            // Set up all views to listen for the click
            this.clickListener = new WeakReference<>(clickListener);
            view.setOnClickListener(this);
            title.setOnClickListener(this);
            thumb.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            this.clickListener.get().onItemClick(getAdapterPosition());
        }
    }
}
