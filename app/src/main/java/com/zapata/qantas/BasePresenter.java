/* Taken from  https://github.com/googlesamples/android-architecture/tree/todo-mvp-dagger/ */

package com.zapata.qantas;

public interface BasePresenter<T> {

    /**
     * Binds presenter with a view when resumed. The Presenter will perform initialization here.
     *
     * @param view the view associated with this presenter
     */
    void takeView(T view);

    /**
     * Drops the reference to the view when destroyed
     */
    void dropView();
}
