package com.zapata.qantas.recipedetail;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.zapata.qantas.R;
import com.zapata.qantas.data.Recipe;
import com.zapata.qantas.recipes.RecipesFragment;
import com.zapata.qantas.recipeweb.RecipeWebviewActivity;
import com.zapata.qantas.util.ActivityUtils;

import javax.inject.Inject;

import dagger.android.support.DaggerAppCompatActivity;

public class RecipeDetailActivity extends AppCompatActivity implements RecipeDetailFragment.LinkClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail_act);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");

        Recipe recipe = getIntent().getParcelableExtra(RecipesFragment.RECIPE_ITEM);
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.contentFrame, RecipeDetailFragment.newInstance(recipe))
                .commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch ( item.getItemId() ) {
            case android.R.id.home:
                finish();
                overridePendingTransition(R.anim.stay, R.anim.slide_right_exit);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.stay, R.anim.slide_right_exit);
    }

    @Override
    public void onLinkClick(Recipe recipe) {
        Intent intent = new Intent(this, RecipeWebviewActivity.class);
        intent.putExtra(RecipeDetailFragment.KEY_RECIPE, recipe);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_up_enter, R.anim.stay);
    }
}
