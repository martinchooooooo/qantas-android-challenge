package com.zapata.qantas.recipedetail;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.zapata.qantas.R;
import com.zapata.qantas.data.Recipe;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RecipeDetailFragment extends Fragment {

    public interface LinkClickListener {
        void onLinkClick(Recipe recipe);
    }

    public static final String KEY_RECIPE = "RECIPE";

    private LinkClickListener mlinkClickListener;
    private Recipe mData;

    @BindView(R.id.txt_title) TextView mTxt_title;
    @BindView(R.id.txt_link) TextView mTxt_link;
    @BindView(R.id.list_ingredients) ListView mlist_ingredients;

    public static RecipeDetailFragment newInstance(Recipe recipe) {
        RecipeDetailFragment rtn = new RecipeDetailFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(KEY_RECIPE, recipe);
        rtn.setArguments(bundle);
        return rtn;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.detail_frag, container, false);
        ButterKnife.bind(this, root);

        Bundle argsBundle = this.getArguments();
        mData = (Recipe) argsBundle.getParcelable(KEY_RECIPE);
        bindData(mData);

        return root;
    }

    private void bindData(Recipe recipe) {
        mTxt_title.setText(recipe.getTitle());

        mTxt_link.setText(recipe.getHref());
        mTxt_link.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mlinkClickListener.onLinkClick(mData);
            }
        });

        ArrayAdapter ingAdapter = new ArrayAdapter(getActivity(), android.R.layout.simple_list_item_1, recipe.getIngredients());
        mlist_ingredients.setAdapter(ingAdapter);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            mlinkClickListener = (LinkClickListener) context;
        } catch ( ClassCastException e ) {
            throw new ClassCastException(context.toString() + "must implement LinkClickListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mlinkClickListener = null;
    }
}
