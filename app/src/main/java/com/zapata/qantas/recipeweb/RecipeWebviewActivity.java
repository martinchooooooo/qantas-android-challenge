package com.zapata.qantas.recipeweb;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.zapata.qantas.R;
import com.zapata.qantas.data.Recipe;
import com.zapata.qantas.recipedetail.RecipeDetailFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RecipeWebviewActivity extends AppCompatActivity {

    @BindView(R.id.web_recipeWebsite) WebView mWebView;

    private Recipe mData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.recipeweb_act);
        ButterKnife.bind(this);
        mData = getIntent().getParcelableExtra(RecipeDetailFragment.KEY_RECIPE);

        // Show the toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(mData.getTitle());
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close);
    }

    @Override
    protected void onStart() {
        super.onStart();
        // Configure our WebView to run in this webview
        mWebView.getSettings().setJavaScriptEnabled(true);
        // Enable responsive layout
        mWebView.getSettings().setUseWideViewPort(true);
        mWebView.setWebViewClient(new WebViewClient());
        mWebView.loadUrl(mData.getHref());
    }

    @Override
    protected void onPause() {
        mWebView.onPause();
        mWebView.pauseTimers();
        super.onPause();
    }

    @Override
    protected void onResume() {
        mWebView.onResume();
        mWebView.resumeTimers();
        super.onResume();
    }

    @Override
    protected void onStop() {
        mWebView.loadUrl("bout:blank");
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        mWebView.destroy();
        mWebView = null;
        super.onDestroy();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch ( item.getItemId() ) {
            case android.R.id.home:
                finish();
                overridePendingTransition(R.anim.stay, R.anim.slide_down_exit);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.stay, R.anim.slide_down_exit);
    }
}
