package com.zapata.qantas.data.source;

import android.support.annotation.NonNull;

import java.lang.annotation.Documented;

import javax.inject.Inject;
import javax.inject.Qualifier;
import javax.inject.Singleton;

@Singleton
public class RecipesRepository implements RecipesDataSource {

    private final RecipesDataSource mRecipesRemoteDataSource;

    @Inject
    RecipesRepository(RecipesDataSource recipesRemoteDataSource) {
        mRecipesRemoteDataSource = recipesRemoteDataSource;
    }

    @Override
    public void getRecipes(@NonNull RecipesDataSource.LoadRecipesCallback callback) {
        mRecipesRemoteDataSource.getRecipes(callback);
    }
}
