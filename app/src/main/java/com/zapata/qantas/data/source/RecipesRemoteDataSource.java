package com.zapata.qantas.data.source;

import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.zapata.qantas.data.Recipe;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.List;

import javax.inject.Singleton;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

@Singleton
public class RecipesRemoteDataSource implements RecipesDataSource {

    private final OkHttpClient client = new OkHttpClient();
    private static String ENDPOINT = "https://g525204.github.io/recipes.json";

    public RecipesRemoteDataSource() {}

    @Override
    public void getRecipes(@NonNull final LoadRecipesCallback loadRecipesCallback) {

        final Handler handler = new Handler(Looper.myLooper());
        final Request request = new Request.Builder()
                .url(ENDPOINT)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override public void onFailure(Call call, IOException e) {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        loadRecipesCallback.onFailure();
                    }
                });
            }

            @Override public void onResponse(Call call, Response response) throws IOException {
                if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);

                String rawResponse = response.body().string();
                JsonObject responseJson = new Gson().fromJson(rawResponse, JsonObject.class);
                JsonArray results = responseJson.getAsJsonArray("results");
                final List<Recipe> recipes = parseData(results);

                // Run on the UI thread
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        loadRecipesCallback.onRecipesLoaded(recipes);
                    }
                });

            }
        });
    }

    /**
     * Parses given {@code recipes} into our Recipe's data.
     * Place appropriate business logic here in this method, e.g. identifying which recipes are
     * deemed popular.
     * @param recipes
     * @return
     */
     public List<Recipe> parseData(JsonArray recipes) {
        Type listType = new TypeToken<List<Recipe>>(){}.getType();

        List<Recipe> rtn = new Gson().fromJson(recipes, listType);

        // Our 'popular' recipes are only those that have an image
        for ( Recipe r : rtn ) {
            if ( r.getThumbnail() != null && !r.getThumbnail().isEmpty() ) {
                r.setPopular(true);
            }

            // Remove any leading or trailing whitespace in the titles
            r.setTitle(r.getTitle().trim());
        }

        return rtn;
    }
}


