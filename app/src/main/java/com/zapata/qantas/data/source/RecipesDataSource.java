package com.zapata.qantas.data.source;

import android.support.annotation.NonNull;

import com.zapata.qantas.data.Recipe;

import java.util.List;

public interface RecipesDataSource {

    interface LoadRecipesCallback {

        void onRecipesLoaded(List<Recipe> recipes);
        void onFailure();
    }

    void getRecipes(@NonNull LoadRecipesCallback callback);
}
