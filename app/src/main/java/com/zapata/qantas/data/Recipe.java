package com.zapata.qantas.data;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.annotations.JsonAdapter;

import java.io.Serializable;
import java.lang.reflect.Type;

public class Recipe implements Parcelable {

    private String title, href, thumbnail;
    private boolean isPopular;

    @JsonAdapter(IngredientsDeserializer.class)
    private String[] ingredients;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String[] getIngredients() {
        return ingredients;
    }

    public void setIngredients(String[] ingredients) {
        this.ingredients = ingredients;
    }

    public boolean isPopular() {
        return isPopular;
    }

    public void setPopular(boolean popular) {
        isPopular = popular;
    }

    // Parcelable implementatin

    public Recipe() {}

    public Recipe(Parcel pc) {
        this.title = pc.readString();
        this.href = pc.readString();
        this.thumbnail = pc.readString();
        this.ingredients = pc.createStringArray();
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(title);
        parcel.writeString(href);
        parcel.writeString(thumbnail);
        parcel.writeStringArray(ingredients);
    }

    public static final Parcelable.Creator<Recipe> CREATOR = new Parcelable.Creator<Recipe>() {
        public Recipe createFromParcel(Parcel pc) { return new Recipe(pc); }
        public Recipe[] newArray(int size) { return new Recipe[size]; }
    };

    @Override
    public int describeContents() {
        return 0;
    }
}

class IngredientsDeserializer implements JsonDeserializer<String[]> {

    @Override
    public String[] deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        String ingredients = json.getAsString();
        if ( ingredients == null &&  ingredients.isEmpty() ) {
            return new String[0];
        } else {
            return ingredients.split(", ");
        }
    }
}
