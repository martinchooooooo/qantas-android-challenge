package com.zapata.qantas;


import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.zapata.qantas.data.Recipe;
import com.zapata.qantas.data.source.RecipesRemoteDataSource;

import org.junit.Test;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class RecipeDataServiceUnitTest {

    /**
     * Test that we are able to properly parse the recipe data and create proper POJO's from our input.
     *
     */
    @Test
    public void loadJson_normalCase() {
        JsonArray recipes = loadSampleRecipeData("recipe_gist.json");
        List<Recipe> recipeItems = new RecipesRemoteDataSource().parseData(recipes);

        assertEquals(recipeItems.size(), 28);

        int popularSize = 0;
        for ( Recipe r : recipeItems ) {
            if ( r.isPopular() ) { popularSize += 1; }
        }
        assertEquals(popularSize, 15);

    }

    @Test
    public void parseIngredients() {
        List<Recipe> recipeItems = new RecipesRemoteDataSource().parseData(loadSampleRecipeData("recipe_gist.json"));

        for ( Recipe r : recipeItems ) {
            int ings = r.getIngredients().length;
            switch ( r.getTitle() ) {
                case "Spaghetti with Clams & Corn":
                    assertEquals(ings, 1); break;
                case "Crock Pot Caramelized Onions":
                    assertEquals(ings, 2); break;
                case "Garlic Vinegar":
                    assertEquals(ings, 3); break;
            }
        }
    }

    @Test
    public void testTrimimingTitleWhitespace() {
        List<Recipe> recipeItems = new RecipesRemoteDataSource().parseData(loadSampleRecipeData("recipe_gist.json"));

        for ( Recipe r : recipeItems ) {
            String t = r.getTitle();
            switch ( r.getHref() ) {
                case "http://cookeatshare.com/recipes/broccoli-casserole-59082":
                    assertEquals("Broccoli Casserole", t); break;
                case "http://www.kraftfoods.com/kf/recipes/garlic-dijon-grilling-sauce-56449.aspx":
                    assertEquals("Garlic Dijon Grilling Sauce", t); break;
            }
        }
    }

    private JsonArray loadSampleRecipeData(String testFile) {
        InputStream is = getClass().getClassLoader().getResourceAsStream(testFile);
        BufferedReader bfr = null;
        try {
            bfr = new BufferedReader(new InputStreamReader(is, "UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        JsonElement jsonElem = new Gson().fromJson(bfr, JsonElement.class);
        JsonObject json = jsonElem.getAsJsonObject();
        return json.getAsJsonArray("results");
    }
}
