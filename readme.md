# Qantas Android Challenge - Recipes

Hiring homework for Qantas

## Building and testing

There are two flavours we're using, prod and mock. Mock reads from the file system for it's data (useful for testing old devices) whereas prod get's data from github directly.

`./gradlew build`

As for tests, run for both flavours but there is only really one implementation.

`./gradlew test`


## Thoughts/Ideas/Comments 

- Going to follow a rough MVP apporach to building the app, allowing for flexibility in the future, in particular with the use of fragments for tablets. e.g. perhaps in landscape mode on a tablet, we could display the recipe list and details side by side?
- Draw inspiration from [this Google Sample](https://github.com/googlesamples/android-architecture/tree/todo-mvp/) as a reference.
- Targettig API 14, which according to Android Studio is 100% of devices. I'm going to assume this a requirement for Qantas given their large userbase.

### Using the `todo-mvp-dagger` template

- After implementing the RecipeDataService, it seemed ripe for using dependency injection, especially for mocking like I was in my test
- The added bonus that there was a template on googlesample's repo was a bonus for dealing with boilerplate
- Removed the local vs remote data sources, we are only going to be reading from the gist url with no caching, db access etc
- Removed the AppExecutors and ThreadExecutors as they seemed a little overkill for this demo. However if we were pretty resource intensive it would definitely be a good idea to utilize the pattern, to avoid bad user interaction when using the app.

### Mocking data by reading from file

In at attempt to mock the recipe data by reading from file, I was able to inject the applicaiton into our FakeDataSource, however because it wasn't a started application it wasn't able to read from the assets directory. I was trying to understand why, however I realised I wasn't getting anywhere and other things to focus on. So I went gave in and read from a string for now. Though its not the nicest solution, it works for our purposes, which is what I needed.

### Popular and Other recipes as a single fragment

These could have been separated into separate fragments, though I didn't see a need for it. Logically, the recipe list is seen as a single list. I did however try to separate adapters and the presenter's calls to updating it, which although may be a little more verbose, would make it easier to refactor out if in the future a feature is needed where separating into two fragments woudl be benefitil. An examople could be having a config option to show popular recipes as list instead of thumbnails.

## TODO's and bugs

There are some outstanding things I would like to have kept working on, though I have to submit this to you guys at some point.
- Clicking on a recipe URL then coming back
- The recipes don't load from github on devices older than android 4.3 because they don't support TLS 1.2. In a prod use case, we could either proxy the data ourselves or have it loaded into the APK (like our mock case)
- Write better tests. Mock the data sources for unit tests, write some integration tests for communicating with github and some tests to make sure that data flows through our presenter properly from our data sources through to our views.


# Thanks!

Thanks for reviewing

Martin